#!/bin/bash
set -o pipefail

cwd=$(pwd)
output="patched-ramdisk.gz"

display_usage() {
	echo -e "\nUsage:\n ./apply-patches.sh [stock_ramdisk]\n"
}

if [ -z "$1" ]; then
  display_usage
	exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]; then 
	display_usage
	exit 0
fi

release=$(git branch --show-current)
echo "Applying patches for release: $release"

if [ ! -f "$1" ]; then
  echo "Input ramdisk not found!"
	exit 1
fi

# Create a tmp dir to hold our extracted files
tmpdir=$(mktemp -d 2>/dev/null || mktemp -d -t 'ramdisk-tmp')
trap "rm -rf $tmpdir" EXIT

echo "Extracting to $tmpdir"
zcat "$1" | cpio -idm --directory=$tmpdir --quiet

# Apply Patches
$(cd $tmpdir && git apply ${cwd}/patches/*.diff)
if [ $? -ne 0 ]; then
  echo "Failed to apply patches. Please make sure the supplied ramdisk matches the patch version"
  exit 1
fi

# Repack
$(cd $tmpdir && find . | cpio -o -H newc -R 0:0 --quiet | gzip -9 > "$cwd/$output")
if [ $? -ne 0 ]; then
  echo "Failed to repack ramdisk!"
  exit 1
fi

echo "Successfully patched ramdisk at: $cwd/$output"
