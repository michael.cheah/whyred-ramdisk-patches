# whyred ramdisk patches

## Usage
1. Copy stock ramdisk to this folder
2. Checkout the branch that's matching the stock ramdisk version. Eg:
    ```
    git checkout V11.0.2.0.PEICNXM_20191023.0000.00_9.0_cn_8072525c20
    ```
3. Run `apply-patches.sh` on the stock ramdisk. Eg:

    ```
    ./apply-patches.sh stock-ramdisk.gz
    ```
4. Output will be found at `patched-ramdisk.gz`
